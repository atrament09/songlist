import React, {useState} from "react";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';


function OneSong({
  name, autor,
  onRemove: handleRemoveSong,
  onEdit: handleEdit,
  isEditMode,
  onOkEdit: handleOkEdit,
  onEditedSong: handleEditedSong }) {
  const [song, setSong] = useState({
      title: name,
      artist: autor
  });

  const handleInputChange = (e) => {
    const value = e.target.value;
    setSong({
        ...song,
        [e.target.name]: value
    })
}

  return (
    <div>
      <div className={isEditMode ? "viewStyle" : ""}>
        <li>{name} - {autor} <DeleteForeverIcon onClick={handleRemoveSong} />
          <button onClick={handleEdit}> Edit </button>
        </li>
      </div>
      <div className={isEditMode ? "" : "editStyle"}>
        <input
          className="editModeInput"
          name="title"
          placeholder="Wpisz nazwę piosenki"
          onChange={handleInputChange}
          value={song.title}
        />
        <input
          className="editModeInput"
          name="artist"
          placeholder="Wpisz wykonawcę"
          onChange={handleInputChange}
          value={song.artist}
        />
        <button onClick={(song) => handleOkEdit}>OK</button>
        <button onClick={()=>handleEdit}>stop</button>
      </div>
    </div>)
}

export default OneSong