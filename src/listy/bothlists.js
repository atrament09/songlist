import React, { useState, useEffect } from "react";
import "./bothlists.css";
import { Container, CssBaseline, Grid, Button } from '@material-ui/core';
import uuid from "uuid";
import axios from 'axios';
import OneSong from "./onesong";


const SONGS_URL = 'http://localhost:3004/songslist';

function BothLists() {
    const [song, setSong] = useState({
        title: "",
        artist: ""
    });
    const [editedSong, setEditedSong] = useState("");
    const [songlist, setSonglist] = useState([]);
    const [isUpdated, setIsUpdated] = useState(false);

    const handleInputChange = (e) => {
        const value = e.target.value;
        setSong({
            ...song,
            [e.target.name]: value
        })
    }

    const handleEditedSong = (e) => {
        const { value } = e.target.value;
        setEditedSong(value)
        console.log(editedSong)
    }

    const handleAddToWhiteList = () => {
        const newItem = { name: song.title, artist: song.artist, id: uuid(), list: "white" };
        axios.post(SONGS_URL, newItem)
            .then(() => {
                setSonglist([...songlist, newItem]);
            })
            .catch((error) => {
                alert('ERROR!');
                console.error(error);
            })
    }
    const handleAddToBlackList = () => {
        const newItem = { name: song.title, artist: song.artist, id: uuid(), list: "black" };
        axios.post(SONGS_URL, newItem)
            .then(() => {
                setSonglist([...songlist, newItem]);
            })
            .catch((error) => {
                alert('ERROR!');
                console.error(error);
            })
    }

    const handleRemoveSong = (item) => () => {
        axios.delete(`${SONGS_URL}/${item.id}`)
            .then(() => {
                setSonglist([...songlist.filter((t) => t !== item)])
            })
            .catch((error) => {
                alert('ERROR!');
                console.error(error);
            })
    }

    const handleOkEdit = (item) => {
        console.log({ item })
    }

    const handleEdit = (item) => () => {
        axios.patch(`${SONGS_URL}/${item.id}`, { editMode: !item.editMode })
            .then((response) => {
                setSonglist(songlist.map(e => {
                    if (e === item) {
                        e.editMode = !e.editMode
                    } return e
                }
                ))
                console.log(item.editMode)
            })
            .catch((error) => {
                alert('ERROR!');
                console.error(error);
            })
    }

    useEffect(() => {
        if (!isUpdated) {
            axios.get(SONGS_URL)
                .then((response) => {
                    setIsUpdated(true);
                    setSonglist(response.data)
                })
                .catch(console.error)
        }
    }, [isUpdated, songlist])

    return (
        <div>
            <CssBaseline />
            <Container maxWidth="md">
                <h1 className="title">Lista piosenek na wesele Kulów</h1>
                <Grid container spacing={2}>
                    <Grid className="input-field" item xs={12}>
                        <form>
                            <input
                                name="title"
                                placeholder="Wpisz nazwę piosenki"
                                onChange={handleInputChange}
                            />

                            <input
                                name="artist"
                                placeholder="Wpisz wykonawcę"
                                onChange={handleInputChange} />
                        </form>
                        <Button
                            variant="contained"
                            onClick={handleAddToWhiteList}
                        >Biała Lista</Button>

                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={handleAddToBlackList}
                        >Czarna Lista</Button>
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid className="white" item xs={6}>
                        <h2>BIAŁA LISTA</h2>
                        {songlist.filter(f => f.list === "white")
                            .map(w => <OneSong
                                name={w.name}
                                key={w.id}
                                autor={w.artist}
                                onRemove={handleRemoveSong(w)}
                                onEdit={handleEdit(w)}
                                isEditMode={w.editMode}
                                onOkEdit={handleOkEdit}
                                onEditedSong={handleEditedSong}
                                 />)}
                    </Grid>
                    <Grid className="black" item xs={6}>
                        <h2>CZARNA LISTA</h2>
                        {songlist.filter(f => f.list === "black")
                            .map(w => <OneSong
                                name={w.name}
                                key={w.id}
                                autor={w.artist}
                                onRemove={handleRemoveSong(w)}
                                onEdit={handleEdit(w)}
                                isEditMode={w.editMode}
                                onOkEdit={handleOkEdit}
                                onEditedSong={handleEditedSong} 
                                />)}
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}

export default BothLists